#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <Cordova/CDVPlugin.h>

@interface iSpyGeo : CDVPlugin <CLLocationManagerDelegate>{
    BOOL __locationStarted;
    NSDate   *lastSentTime;
    NSDate   *lastLocationTime;
    NSDate   *lastBabysitTime;
    NSRecursiveLock *lock;
}

@property (nonatomic, strong) CLLocationManager* locationManager;
@property (nonatomic, strong) CLRegion* geofence;
@property (nonatomic, strong) NSMutableDictionary* lastLocation;
@property (nonatomic, strong) NSMutableDictionary* options;
@property (nonatomic, copy)   NSString *callbackId;
@property (nonatomic, copy)   NSString *locationCallback;
@property (nonatomic, copy)   NSString *messageCallback;
@property (nonatomic, copy)   NSString *ispyUrl;
@property (nonatomic, copy)   NSString *ispyAgency;
@property (nonatomic, copy)   NSString *ispyUserId;
@property (nonatomic, copy)   NSString *ispyUserRadioNumber;
@property (nonatomic, copy)   NSString *ispyCallId;
@property (nonatomic, copy)   NSString *ispyLongTermCallId;
@property (nonatomic, assign) BOOL isapparatus;
@property (nonatomic, assign) BOOL isAVL;
@property (nonatomic, assign) BOOL isAVLArrived;
@property (nonatomic, assign) BOOL isTracking;
@property (nonatomic, assign) BOOL isSignificant;
@property (nonatomic)         CLLocationDegrees ispyLongitude;
@property (nonatomic)         CLLocationDegrees ispyLatitude;

- (void)startTracking:(CDVInvokedUrlCommand*)command;
- (void)stopTracking:(CDVInvokedUrlCommand*)command;
- (void)arrivedTracking:(CDVInvokedUrlCommand*)command;
- (void)arrivedTrackingNoLock:(CDVInvokedUrlCommand*)command;
- (void)startAVLTracking:(CDVInvokedUrlCommand*)command;
- (void)stopAVLTracking:(CDVInvokedUrlCommand*)command;
- (void)preventSleep:(CDVInvokedUrlCommand*)command;
- (void)allowSleep:(CDVInvokedUrlCommand*)command;

- (void)locationManager:(CLLocationManager*)manager
    didUpdateToLocation:(CLLocation*)newLocation
           fromLocation:(CLLocation*)oldLocation;

- (void)locationManager:(CLLocationManager*)manager
       didFailWithError:(NSError*)error;

- (BOOL)isLocationServicesEnabled;

- (NSDictionary*)locationToDictionary:(CLLocation*)location;

- (void)updateLocation:(BOOL)forceSend;

- (void)sendStringMessage:(NSString*)message
            callBack:(NSString*)callback;
- (void)sendDictionaryMessage:(NSMutableDictionary*)dictionary
            callBack:(NSString*)callback;
@end
