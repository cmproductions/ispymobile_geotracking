#import "iSpyGeo.h"

#define kPGLocationErrorDomain @"kPGLocationErrorDomain"
#define kPGLocationDesiredAccuracyKey @"desiredAccuracy"
#define kPGLocationForcePromptKey @"forcePrompt"
#define kPGLocationDistanceFilterKey @"distanceFilter"
#define kPGLocationFrequencyKey @"frequency"

@implementation iSpyGeo

@synthesize locationManager;
@synthesize lastLocation;
@synthesize callbackId;
@synthesize locationCallback;
@synthesize messageCallback;
@synthesize ispyUrl;
@synthesize ispyAgency;
@synthesize ispyUserId;
@synthesize ispyUserRadioNumber;
@synthesize ispyCallId;
@synthesize ispyLongTermCallId;
@synthesize ispyLongitude;
@synthesize ispyLatitude;
@synthesize isapparatus;
@synthesize isAVL;
@synthesize isAVLArrived;
@synthesize isTracking;
@synthesize isSignificant;

- (void)pluginInitialize
{
    self.locationManager = [[CLLocationManager alloc] init];
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
    {
        [self.locationManager requestWhenInUseAuthorization];
    }
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 9.0)
    {
        self.locationManager.allowsBackgroundLocationUpdates = YES;
    }
    self.locationManager.delegate = self;
    self.isAVL = NO;
    self.isAVLArrived = NO;
    self.isTracking = NO;
    self.isSignificant = NO;
    
    lastBabysitTime = [NSDate date];
}

- (BOOL)isAuthorized
{
    BOOL authorizationStatusClassPropertyAvailable = [CLLocationManager respondsToSelector:@selector(authorizationStatus)]; // iOS 4.2+
    
    if (authorizationStatusClassPropertyAvailable) {
        NSUInteger authStatus = [CLLocationManager authorizationStatus];
        if (authStatus != kCLAuthorizationStatusAuthorizedAlways) {
            [self.locationManager requestAlwaysAuthorization];
        }
        return (authStatus == kCLAuthorizationStatusAuthorizedAlways) || (authStatus == kCLAuthorizationStatusAuthorizedWhenInUse) || (authStatus == kCLAuthorizationStatusNotDetermined);
    }
    
    // by default, assume YES (for iOS < 4.2)
    return YES;
}

- (BOOL)isLocationServicesEnabled
{
    BOOL locationServicesEnabledInstancePropertyAvailable = [self.locationManager respondsToSelector:@selector(locationServicesEnabled)]; // iOS 3.x
    BOOL locationServicesEnabledClassPropertyAvailable = [CLLocationManager respondsToSelector:@selector(locationServicesEnabled)]; // iOS 4.x

    if (locationServicesEnabledClassPropertyAvailable) { // iOS 4.x
        return [CLLocationManager locationServicesEnabled];
    } else if (locationServicesEnabledInstancePropertyAvailable) { // iOS 2.x, iOS 3.x
        return [(id)self.locationManager locationServicesEnabled];
    } else {
        return NO;
    }
}

- (void)startTracking:(CDVInvokedUrlCommand*)command
{
    if (command.callbackId == NULL) {
        NSLog(@"No callback passed");
        return;
    }
    
    if (!self.isAVL) {
        if (![self isLocationServicesEnabled]) {
            NSLog(@"locationManager::startLocation - Location services are not enabled.");
            return;
        }
        if (![self isAuthorized]) {
            NSString* message = nil;
            BOOL authStatusAvailable = [CLLocationManager respondsToSelector:@selector(authorizationStatus)]; // iOS 4.2+
            if (authStatusAvailable) {
                NSUInteger code = [CLLocationManager authorizationStatus];
                if (code == kCLAuthorizationStatusNotDetermined) {
                    // could return POSITION_UNAVAILABLE but need to coordinate with other platforms
                    message = @"User undecided on application's use of location services.";
                } else if (code == kCLAuthorizationStatusRestricted) {
                    message = @"Application's use of location services is restricted.";
                }
            }
            // PERMISSIONDENIED is only PositionError that makes sense when authorization denied
            NSLog(@"locationManager::startLocation - %@", message);

            return;
        }
        if (__locationStarted) {
            [self stopTracking:nil];
        }

        self.options = [command.arguments objectAtIndex:0];
        self.ispyAgency = [self.options objectForKey:@"agency"];
        self.callbackId = command.callbackId;
        self.locationCallback = [self.options objectForKey:@"lcb"];
        self.messageCallback = [self.options objectForKey:@"mcb"];
        self.ispyUrl = [self.options objectForKey:@"url"];
        self.ispyUserId = [self.options objectForKey:@"userid"];
        self.isapparatus = [[self.options objectForKey:@"isApparatus"] boolValue];
        self.ispyUserRadioNumber = [self.options objectForKey:@"userRadioNumber"];
        self.locationManager.distanceFilter = kCLDistanceFilterNone;
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation;
        [self.locationManager startUpdatingLocation];
    }
    
    self.ispyCallId = [[command.arguments objectAtIndex:0] objectForKey:@"callid"];
    self.ispyLongTermCallId = [[command.arguments objectAtIndex:0] objectForKey:@"longTermCallId"];
    self.ispyLongitude = [[[command.arguments objectAtIndex:0] objectForKey:@"long"] doubleValue];
    self.ispyLatitude = [[[command.arguments objectAtIndex:0] objectForKey:@"lat"] doubleValue];
    
    [self startGeoFence];
    __locationStarted = YES;
    self.isTracking = YES;
    
    [self sendStringMessage:@"Started tracking" callBack:self.messageCallback];
    
    NSLog(@"Started tracking");
}

- (void)startAVLTracking:(CDVInvokedUrlCommand*)command
{
    if (![self isLocationServicesEnabled]) {
        NSLog(@"locationManager::startLocation - Location services are not enabled.");
        return;
    }
    if (![self isAuthorized]) {
        NSString* message = nil;
        BOOL authStatusAvailable = [CLLocationManager respondsToSelector:@selector(authorizationStatus)]; // iOS 4.2+
        if (authStatusAvailable) {
            NSUInteger code = [CLLocationManager authorizationStatus];
            if (code == kCLAuthorizationStatusNotDetermined) {
                // could return POSITION_UNAVAILABLE but need to coordinate with other platforms
                message = @"User undecided on application's use of location services.";
            } else if (code == kCLAuthorizationStatusRestricted) {
                message = @"Application's use of location services is restricted.";
            }
        }
        // PERMISSIONDENIED is only PositionError that makes sense when authorization denied
        NSLog(@"locationManager::startLocation - %@", message);

        return;
    }
    if (command.callbackId == NULL) {
        NSLog(@"No callback passed");
        return;
    }
    if (__locationStarted) {
        [self stopTracking:nil];
    }

    self.options = [command.arguments objectAtIndex:0];
    self.callbackId = command.callbackId;
    self.ispyAgency = [self.options objectForKey:@"agency"];
    self.locationCallback = [self.options objectForKey:@"lcb"];
    self.messageCallback = [self.options objectForKey:@"mcb"];
    self.ispyUrl = [self.options objectForKey:@"url"];
    self.ispyUserId = [self.options objectForKey:@"userid"];
    self.ispyUserRadioNumber = [self.options objectForKey:@"userRadioNumber"];
    self.isapparatus = [[self.options objectForKey:@"isApparatus"] boolValue];
    self.locationManager.distanceFilter = kCLDistanceFilterNone;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation;
    self.locationManager.pausesLocationUpdatesAutomatically = NO;
    [self.locationManager startUpdatingLocation];
    self.isAVL = YES;
    __locationStarted = YES;

    [self sendStringMessage:@"Started AVL tracking" callBack:self.messageCallback];

    NSLog(@"Started AVL tracking");
}

- (void)stopAVLTracking:(CDVInvokedUrlCommand*)command
{
    NSLog(@"Stopping AVL tracking");
    self.isAVL = NO;
    [self stopTracking:command];
}

- (void)willEnterForeground:(UIApplication *)application
{
    if (self.isSignificant) {
        self.isSignificant = NO;
        [self.locationManager stopMonitoringSignificantLocationChanges];
        [self.locationManager startUpdatingLocation];
    }
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    if (self.isAVL && !self.isTracking) {
        self.isSignificant = YES;
        [self.locationManager stopUpdatingLocation];
        [self.locationManager startMonitoringSignificantLocationChanges];
    }
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
//    if ([launchOptions objectForKey:UIApplicationLaunchOptionsLocationKey]) {
        if (self.isAVL && self.isSignificant) {
            self.isSignificant = NO;
            [self.locationManager stopMonitoringSignificantLocationChanges];
            [self.locationManager startUpdatingLocation];
        }
//    }
    
    return YES;
}

- (void)stopTracking:(CDVInvokedUrlCommand*)command
{
    if (__locationStarted) {
        if (![self isLocationServicesEnabled]) {
            return;
        }

        if (!self.isAVL) {
            [self.locationManager stopUpdatingLocation];
            [self sendStringMessage:@"Stopped tracking" callBack:self.messageCallback];
            lastSentTime = nil;
            lastLocationTime = nil;
            self.lastLocation = nil;
            self.callbackId = nil;
            self.locationCallback = nil;
            self.messageCallback = nil;
            self.ispyUrl = nil;
            self.ispyUserId = nil;
            self.isapparatus = NO;
            __locationStarted = NO;
        }

        self.isTracking = NO;
        self.ispyCallId = nil;
        self.ispyLongitude = 0;
        self.ispyLatitude = 0;

        NSLog(@"Stopped tracking");
    }
}

- (void)arrivedTracking:(CDVInvokedUrlCommand*)command
{
    if (self.isTracking || (self.ispyLongitude != 0 && self.ispyLatitude != 0))
    {
        [self->lock lock];
        [self.lastLocation setObject:[NSNumber numberWithBool:YES] forKey:@"arrived"];
        [self.lastLocation setObject:[NSNumber numberWithDouble:self.ispyLatitude] forKey:@"latitude"];
        [self.lastLocation setObject:[NSNumber numberWithDouble:self.ispyLongitude] forKey:@"longitude"];
        [self sendDictionaryMessage:self.lastLocation callBack:self.locationCallback];
        [self updateLocation:YES];
        [self.locationManager stopMonitoringForRegion:self.geofence];
        [self->lock unlock];
    }
    
    [self stopTracking:nil];
}

- (void)arrivedTrackingNoLock:(CDVInvokedUrlCommand *)command
{
    [self.locationManager stopMonitoringForRegion:self.geofence];
    [self stopTracking:nil];
}

- (void)preventSleep:(CDVInvokedUrlCommand*)command
{
    [UIApplication sharedApplication].idleTimerDisabled = YES;
}

- (void)allowSleep:(CDVInvokedUrlCommand*)command
{
    [UIApplication sharedApplication].idleTimerDisabled = NO;
}

- (void) startGeoFence
{
    NSString *title = @"com.ispyfire.plugins.geotracking.ProximityAlert";

    CLLocationDegrees latitude = self.ispyLatitude;
    CLLocationDegrees longitude = self.ispyLongitude;
    CLLocationCoordinate2D centerCoordinate = CLLocationCoordinate2DMake(latitude, longitude);

    CLLocationDistance regionRadius = 110;

    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)
    {
        CLCircularRegion *region = [[CLCircularRegion alloc] initWithCenter:centerCoordinate radius:regionRadius identifier:title];
        self.geofence = region;
    }
    else
    {
        self.geofence = [[CLRegion alloc] initCircularRegionWithCenter:centerCoordinate radius:regionRadius identifier:title];
    }
}

- (void)locationManager:(CLLocationManager*)manager
    didUpdateToLocation:(CLLocation*)newLocation
           fromLocation:(CLLocation*)oldLocation
{
    if (UIApplication.sharedApplication.applicationState == UIApplicationStateActive) {
        NSLog(@"App is in the foreground. New location is %@", newLocation);
    } else {
        NSLog(@"App is backgrounded. New location is %@", newLocation);
    }
    [self->lock lock];
    [self locationToDictionary:newLocation];
    [self sendDictionaryMessage:self.lastLocation callBack:self.locationCallback];
    [self updateLocation:NO];
    [self->lock unlock];
}

- (void)locationManager:(CLLocationManager*)manager didFailWithError:(NSError*)error
{
    if([error code] == 0)
    {
        return;
    }
    
    NSLog(@"locationManager::didFailWithError %@", [error localizedFailureReason]);
    NSLog(@"locationManager::didFailWithError %@", error);

    [self stopTracking:nil];
}

- (void)locationManager:(CLLocationManager *)manager didEnterRegion:(CLRegion *)region {
    NSLog(@"locationManager::didEnterRegion - %@", region.identifier);
}

- (void)locationManager:(CLLocationManager *)manager didExitRegion:(CLRegion *)region {
    NSLog(@"locationManager::didExitRegion - %@", region.identifier);
}

- (void)locationManager:(CLLocationManager *)manager monitoringDidFailForRegion:(CLRegion *)region withError:(NSError *)error {
    NSLog(@"locationManager::monitoringDidFailForRegion - %@", error);
    [self stopTracking:nil];
}

- (void)locationManager:(CLLocationManager *)manager didStartMonitoringForRegion:(CLRegion *)region {
    NSLog(@"Started monitoring %@ region", region.identifier);
}

- (void)dealloc
{
    self.locationManager.delegate = nil;
}

- (void)onReset
{
    [self stopTracking:nil];
    [self.locationManager stopUpdatingHeading];
}

- (NSDictionary*)locationToDictionary:(CLLocation*)location {
    if ([self.ispyUserId length] > 0)
    {
        lastLocationTime = location.timestamp;
        self.lastLocation = [NSMutableDictionary dictionary];
        
        [self.lastLocation setObject:[NSNumber numberWithDouble:location.coordinate.latitude] forKey:@"latitude"];
        [self.lastLocation setObject:[NSNumber numberWithDouble:location.coordinate.longitude] forKey:@"longitude"];
        [self.lastLocation setObject:[NSNumber numberWithDouble:[location.timestamp timeIntervalSince1970]] forKey:@"timestamp"];
        [self.lastLocation setObject:[NSNumber numberWithDouble:location.altitude] forKey:@"altitude"];
        [self.lastLocation setObject:[NSNumber numberWithDouble:location.course] forKey:@"course"];
        [self.lastLocation setObject:[NSNumber numberWithDouble:location.speed] forKey:@"speed"];
        [self.lastLocation setObject:[NSNumber numberWithDouble:location.horizontalAccuracy] forKey:@"horizontalAccuracy"];
        [self.lastLocation setObject:[NSNumber numberWithDouble:location.verticalAccuracy] forKey:@"verticalAccuracy"];
        [self.lastLocation setObject:[NSNumber numberWithBool:NO] forKey:@"arrived"];
        [self.lastLocation setObject:self.ispyUserId forKey:@"userid"];
        [self.lastLocation setObject:[NSNumber numberWithBool:self.isapparatus] forKey:@"isApparatus"];
        [self.lastLocation setObject:[NSNumber numberWithBool:self.isAVL] forKey:@"isAVL"];
        if (self.ispyUserRadioNumber) {
            [self.lastLocation setObject:self.ispyUserRadioNumber forKey:@"unitid"];
        } else {
            [self.lastLocation setObject:@"" forKey:@"unitid"];
        }
        
        if (self.ispyCallId!=Nil)
            [self.lastLocation setObject:self.ispyCallId forKey:@"callid"];
        
        if (self.isTracking)
        {
            if ([self.geofence containsCoordinate:location.coordinate]) {
                NSLog(@"locationManager::didEnterRegion - %@", self.geofence.identifier);
                [self->lock lock];
                [self.lastLocation setObject:[NSNumber numberWithBool:YES] forKey:@"arrived"];
                [self.lastLocation setObject:[NSNumber numberWithDouble:self.ispyLatitude] forKey:@"latitude"];
                [self.lastLocation setObject:[NSNumber numberWithDouble:self.ispyLongitude] forKey:@"longitude"];
                [self sendDictionaryMessage:self.lastLocation callBack:self.locationCallback];
                [self updateLocation:YES];
                self.isTracking = NO;
                
                if (self.isAVL)
                    self.isAVLArrived = YES;
                [self->lock unlock];
                [self stopTracking:nil];
            }
        }
        else if (isAVLArrived && self.isAVL && ![self.geofence containsCoordinate:location.coordinate]) {
            self.isAVLArrived = false;
        }
    }
    
    return self.lastLocation;
}

- (void)updateLocation:(BOOL)force {
    NSDate* timeNow = [NSDate date];
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration]; //[NSURLSessionConfiguration backgroundSessionConfigurationWithIdentifier:@"ispyfire"]
    sessionConfig.timeoutIntervalForRequest = 5.0;
    sessionConfig.timeoutIntervalForResource = 5.0;
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig];
    
    if (self.lastLocation != nil &&
        (force || (lastLocationTime != nil && (lastSentTime == nil || [timeNow timeIntervalSinceDate:lastSentTime] > 30.0f) && !self.isAVLArrived)))
    {
        lastSentTime = timeNow;
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:self.ispyUrl]];
        NSError *error;
        NSData *putData = [NSJSONSerialization dataWithJSONObject:self.lastLocation options:0 error:&error];
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [request setHTTPMethod:@"PUT"];
        [request setHTTPBody:putData];
        
        NSLog(@"***** I'M #1 *****");
        NSLog(@"%@", self.ispyUrl);

//        NSError *requestError;
//        NSURLResponse *urlResponse = nil;
//        [NSURLConnection sendSynchronousRequest:request returningResponse:&urlResponse error:&requestError];
        NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) { NSLog(@"***** I'M #2 *****"); }];
        [task resume];
    } else {
        [self sendStringMessage:@"Not long enough to send" callBack:self.messageCallback];
    }
    
    if (lastBabysitTime != nil && [timeNow timeIntervalSinceDate:lastBabysitTime] > 90.0f && self.isTracking)
    {
        lastBabysitTime = timeNow;
        NSString *babysitUrl = [NSString stringWithFormat:@"https://api.ispyfire.com/calls/isopen/%@/%@?_=%ld", self.ispyAgency, self.ispyLongTermCallId, (long)[lastBabysitTime timeIntervalSince1970]];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:babysitUrl]];
        [request setHTTPMethod:@"GET"];
        
        NSLog(@"***** I'M #3 *****");
        NSLog(@"%@", babysitUrl);
        
//        NSError *requestError;
//        NSHTTPURLResponse *urlResponse = nil;
//        [NSURLConnection sendSynchronousRequest:request returningResponse:&urlResponse error:&requestError];
//        
//        if ([urlResponse statusCode] == 404)
//        {
//            NSLog(@"***** I'M #3 *****");
//            [self arrivedTracking:nil];
//        }
        NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            NSHTTPURLResponse *urlResponse = (NSHTTPURLResponse *)response;
            NSLog(@"***** I'M #4 *****");
            NSLog(@"StatusCode: %ld", (long)[urlResponse statusCode]);
            if ([urlResponse statusCode] == 404)
            {
                NSLog(@"***** I'M #5 *****");
                [self arrivedTrackingNoLock:nil];
            }
        }];
        [task resume];
    }
}

- (void)sendStringMessage:(NSString*)message
                callBack:(NSString*)callback
{
    if ([callback length] > 0)
    {
        NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
        [dictionary setObject:message forKey:@"message"];
        
        [self sendDictionaryMessage:dictionary callBack:callback];
    }
}

- (void)sendDictionaryMessage:(NSMutableDictionary*)dictionary
                                  callBack:(NSString*)callback
{
    if (dictionary != nil && [callback length] > 0)
    {
        NSError *error;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dictionary options:0 error:&error];
        NSString *jsonStr = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        NSLog(@"Msg: %@", jsonStr);
        NSString * jsCallBack = [NSString stringWithFormat:@"%@(%@);", callback, jsonStr];
        if ([self.webView isKindOfClass:[UIWebView class]]) {
            [(UIWebView*)self.webView stringByEvaluatingJavaScriptFromString:jsCallBack];
        }
    }
}
@end
