#import "iSpyCurrentLocation.h"

#define kPGLocationErrorDomain @"kPGLocationErrorDomain"
#define kPGLocationDesiredAccuracyKey @"desiredAccuracy"
#define kPGLocationForcePromptKey @"forcePrompt"
#define kPGLocationDistanceFilterKey @"distanceFilter"
#define kPGLocationFrequencyKey @"frequency"

@implementation iSpyCurrentLocation

@synthesize locationManager;
@synthesize callbackId;
@synthesize locationCallback;
@synthesize messageCallback;
@synthesize lastLocation;
@synthesize options;
@synthesize isTracking;

- (void)pluginInitialize
{
    self.locationManager = [[CLLocationManager alloc] init];
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
    {
        [self.locationManager requestWhenInUseAuthorization];
    }
    self.locationManager.delegate = self;
}

- (BOOL)isAuthorized
{
    BOOL authorizationStatusClassPropertyAvailable = [CLLocationManager respondsToSelector:@selector(authorizationStatus)]; // iOS 4.2+
    
    if (authorizationStatusClassPropertyAvailable) {
        NSUInteger authStatus = [CLLocationManager authorizationStatus];
        if (authStatus != kCLAuthorizationStatusAuthorizedAlways) {
            [self.locationManager requestAlwaysAuthorization];
        }
        return (authStatus == kCLAuthorizationStatusAuthorizedAlways) || (authStatus == kCLAuthorizationStatusAuthorizedWhenInUse) || (authStatus == kCLAuthorizationStatusNotDetermined);
    }
    
    // by default, assume YES (for iOS < 4.2)
    return YES;
}

- (BOOL)isLocationServicesEnabled
{
    BOOL locationServicesEnabledInstancePropertyAvailable = [self.locationManager respondsToSelector:@selector(locationServicesEnabled)]; // iOS 3.x
    BOOL locationServicesEnabledClassPropertyAvailable = [CLLocationManager respondsToSelector:@selector(locationServicesEnabled)]; // iOS 4.x
    
    if (locationServicesEnabledClassPropertyAvailable) { // iOS 4.x
        return [CLLocationManager locationServicesEnabled];
    } else if (locationServicesEnabledInstancePropertyAvailable) { // iOS 2.x, iOS 3.x
        return [(id)self.locationManager locationServicesEnabled];
    } else {
        return NO;
    }
}

- (void)getLocation:(CDVInvokedUrlCommand*)command
{
    self.isTracking = NO;
    [self startLocationServices:command];
}

- (void)startTracking:(CDVInvokedUrlCommand *)command
{
    self.isTracking = YES;
    [self startLocationServices:command];
}

- (void)startLocationServices:(CDVInvokedUrlCommand*)command
{
    if (![self isLocationServicesEnabled]) {
        NSLog(@"locationManager::startLocation - Location services are not enabled.");
        return;
    }
    if (![self isAuthorized]) {
        NSString* message = nil;
        BOOL authStatusAvailable = [CLLocationManager respondsToSelector:@selector(authorizationStatus)]; // iOS 4.2+
        if (authStatusAvailable) {
            NSUInteger code = [CLLocationManager authorizationStatus];
            if (code == kCLAuthorizationStatusNotDetermined) {
                // could return POSITION_UNAVAILABLE but need to coordinate with other platforms
                message = @"User undecided on application's use of location services.";
            } else if (code == kCLAuthorizationStatusRestricted) {
                message = @"Application's use of location services is restricted.";
            } else if (code == kCLAuthorizationStatusAuthorizedWhenInUse) {
                message = @"Applications's use of location services is only allowed when using.";
            } else if (code == kCLAuthorizationStatusAuthorizedAlways) {
                message = @"Application's use of location services is always allowed.";
            }
        }
        // PERMISSIONDENIED is only PositionError that makes sense when authorization denied
        NSLog(@"locationManager::startLocation - %@", message);
        
        return;
    }
    if (command.callbackId == NULL) {
        NSLog(@"No callback passed");
        return;
    }
    if (__locationStarted) {
        [self stopTracking:nil];
    }
    
    self.options = [command.arguments objectAtIndex:0];
    self.callbackId = command.callbackId;
    self.locationCallback = [self.options objectForKey:@"lcb"];
    self.messageCallback = [self.options objectForKey:@"mcb"];

    self.locationManager.distanceFilter = kCLDistanceFilterNone;

    if (self.isTracking) {
        self.locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
    }
    else {
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation;
    }
    [self.locationManager startUpdatingLocation];
    __locationStarted = YES;

    [self sendStringMessage:@"Started current location tracking" callBack:self.messageCallback];
    
    NSLog(@"Started current location tracking");
}

- (void)stopTracking:(CDVInvokedUrlCommand*)command
{
    if (__locationStarted) {
        if (![self isLocationServicesEnabled]) {
            return;
        }
        
        [self.locationManager stopUpdatingLocation];
        [self sendStringMessage:@"Stopped current location tracking" callBack:self.messageCallback];
        self.callbackId = nil;
        self.locationCallback = nil;
        self.messageCallback = nil;
        __locationStarted = NO;
    
        NSLog(@"Stopped current tracking");
    }
}


- (void)locationManager:(CLLocationManager*)manager
    didUpdateToLocation:(CLLocation*)newLocation
           fromLocation:(CLLocation*)oldLocation
{
    if (UIApplication.sharedApplication.applicationState == UIApplicationStateActive) {
        NSLog(@"App is in the foreground. New location is %@", newLocation);
    } else {
        NSLog(@"App is backgrounded. New location is %@", newLocation);
    }
    
    if (self.isTracking) {
        if (newLocation.horizontalAccuracy <= 200)
        {
            [self locationToDictionary:newLocation];
            [self sendDictionaryMessage:self.lastLocation callBack:self.locationCallback];
        }
    }
    else {
        if (newLocation.horizontalAccuracy <= 25)
        {
            [self locationToDictionary:newLocation];
            [self sendDictionaryMessage:self.lastLocation callBack:self.locationCallback];
            [self stopTracking:nil];
        }
    }
}

- (void)locationManager:(CLLocationManager*)manager didFailWithError:(NSError*)error
{
    if([error code] == 0)
    {
        return;
    }
    
    NSLog(@"locationManager::didFailWithError %@", [error localizedFailureReason]);
    NSLog(@"locationManager::didFailWithError %@", error);
    
    [self stopTracking:nil];
}

- (void)locationManager:(CLLocationManager *)manager didEnterRegion:(CLRegion *)region {
    NSLog(@"locationManager::didEnterRegion - %@", region.identifier);
}

- (void)locationManager:(CLLocationManager *)manager didExitRegion:(CLRegion *)region {
    NSLog(@"locationManager::didExitRegion - %@", region.identifier);
}

- (void)locationManager:(CLLocationManager *)manager monitoringDidFailForRegion:(CLRegion *)region withError:(NSError *)error {
    NSLog(@"locationManager::monitoringDidFailForRegion - %@", error);
    [self stopTracking:nil];
}

- (void)locationManager:(CLLocationManager *)manager didStartMonitoringForRegion:(CLRegion *)region {
    NSLog(@"Started monitoring %@ region", region.identifier);
}

- (void)dealloc
{
    self.locationManager.delegate = nil;
}

- (void)onReset
{
    [self stopTracking:nil];
    [self.locationManager stopUpdatingHeading];
}

- (NSDictionary*)locationToDictionary:(CLLocation*)location {
    self.lastLocation = [NSMutableDictionary dictionary];
    
    [self.lastLocation setObject:[NSNumber numberWithDouble:location.coordinate.latitude] forKey:@"latitude"];
    [self.lastLocation setObject:[NSNumber numberWithDouble:location.coordinate.longitude] forKey:@"longitude"];
    [self.lastLocation setObject:[NSNumber numberWithDouble:[location.timestamp timeIntervalSince1970]] forKey:@"timestamp"];
    [self.lastLocation setObject:[NSNumber numberWithDouble:location.altitude] forKey:@"altitude"];
    [self.lastLocation setObject:[NSNumber numberWithDouble:location.course] forKey:@"course"];
    [self.lastLocation setObject:[NSNumber numberWithDouble:location.speed] forKey:@"speed"];
    [self.lastLocation setObject:[NSNumber numberWithDouble:location.horizontalAccuracy] forKey:@"horizontalAccuracy"];
    [self.lastLocation setObject:[NSNumber numberWithDouble:location.verticalAccuracy] forKey:@"verticalAccuracy"];
    
    return self.lastLocation;
}

- (void)sendStringMessage:(NSString*)message
                 callBack:(NSString*)callback
{
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
    [dictionary setObject:message forKey:@"message"];
    
    [self sendDictionaryMessage:dictionary callBack:callback];
}

- (void)sendDictionaryMessage:(NSMutableDictionary*)dictionary
                     callBack:(NSString*)callback
{
    if (dictionary != nil)
    {
        NSError *error;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dictionary options:0 error:&error];
        NSString *jsonStr = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        NSLog(@"Msg: %@", jsonStr);
        NSString * jsCallBack = [NSString stringWithFormat:@"%@(%@);", callback, jsonStr];
        if ([self.webView isKindOfClass:[UIWebView class]]) {
            [(UIWebView*)self.webView stringByEvaluatingJavaScriptFromString:jsCallBack];
        }
    }
}
@end
