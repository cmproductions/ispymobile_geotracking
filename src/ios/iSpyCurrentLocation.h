#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <Cordova/CDVPlugin.h>

@interface iSpyCurrentLocation : CDVPlugin <CLLocationManagerDelegate>{
    BOOL __locationStarted;
}

@property (nonatomic, strong) CLLocationManager* locationManager;
@property (nonatomic, copy)   NSString *callbackId;
@property (nonatomic, copy)   NSString *locationCallback;
@property (nonatomic, copy)   NSString *messageCallback;
@property (nonatomic, strong) NSMutableDictionary* lastLocation;
@property (nonatomic, strong) NSMutableDictionary* options;
@property (nonatomic, assign) BOOL isTracking;


- (void)getLocation:(CDVInvokedUrlCommand*)command;
- (void)startTracking:(CDVInvokedUrlCommand*)command;
- (void)stopTracking:(CDVInvokedUrlCommand*)command;
- (void)startLocationServices:(CDVInvokedUrlCommand*)command;

- (void)locationManager:(CLLocationManager*)manager
    didUpdateToLocation:(CLLocation*)newLocation
           fromLocation:(CLLocation*)oldLocation;

- (void)locationManager:(CLLocationManager*)manager
       didFailWithError:(NSError*)error;

- (BOOL)isLocationServicesEnabled;

- (NSDictionary*)locationToDictionary:(CLLocation*)location;

- (void)sendStringMessage:(NSString*)message
                 callBack:(NSString*)callback;

- (void)sendDictionaryMessage:(NSMutableDictionary*)dictionary
                     callBack:(NSString*)callback;
@end
