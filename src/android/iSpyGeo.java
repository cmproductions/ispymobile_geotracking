package com.ispyfire.plugins.geotracking;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaWebView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.location.LocationManager;
import android.util.Log;
import android.os.PowerManager;
import android.os.IBinder;

public class iSpyGeo extends CordovaPlugin {
    private PowerManager powerManager;
    private PowerManager.WakeLock wakeLock;
    private LocationManager locationManager;
    private Intent backgroundLocationIntent;
    private static iSpyLocationListener listener;
	private static CordovaWebView gWebView;
	private static String TAG = "[iSpyMobile Geotracker]";
	private static boolean isAvlRunning;
	private static boolean isAvlEnabledDevice;

	public static iSpyLocationListener getLocationListener() {
	    return listener;
    }

    @SuppressLint("Wakelock")
	@SuppressWarnings("deprecation")
	public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        if (locationManager == null) {
            locationManager = (LocationManager) this.cordova.getActivity().getSystemService(Context.LOCATION_SERVICE);
        }

        if (listener == null) {
            listener = new iSpyLocationListener(locationManager, this);
        }

        if (powerManager == null) {
            powerManager = (PowerManager) this.cordova.getActivity().getSystemService(Context.POWER_SERVICE);
        }

        if (wakeLock == null) {
            wakeLock = powerManager.newWakeLock(PowerManager.SCREEN_BRIGHT_WAKE_LOCK, "iSpyFire");
        }

        if (action.equals("startTracking")) {
            gWebView = this.webView;
            if (backgroundLocationIntent == null) {
                backgroundLocationIntent = new Intent(this.cordova.getActivity().getApplicationContext(), iSpyAVLService.class);
                backgroundLocationIntent.putExtra("jsonargs", args.getJSONObject(0).toString());
            }
            this.cordova.getActivity().getApplicationContext().startService(backgroundLocationIntent);
            this.cordova.getActivity().getApplicationContext().bindService(backgroundLocationIntent, mServiceConn, 0);
        } else if (action.equals("stopTracking")) {
            listener.stopAVLTracking();
            if (backgroundLocationIntent != null) {
                this.cordova.getActivity().getApplicationContext().stopService(backgroundLocationIntent);
                backgroundLocationIntent = null;
            }
        } else if (action.equals("startAVLTracking")) {
            isAvlEnabledDevice = true;
            isAvlRunning = true;
            gWebView = this.webView;
            if (backgroundLocationIntent == null) {
                backgroundLocationIntent = new Intent(this.cordova.getActivity().getApplicationContext(), iSpyAVLService.class);
                backgroundLocationIntent.putExtra("jsonargs", args.getJSONObject(0).toString());
            }
            this.cordova.getActivity().getApplicationContext().startService(backgroundLocationIntent);
            this.cordova.getActivity().getApplicationContext().bindService(backgroundLocationIntent, mServiceConn, 0);
        } else if (action.equals("stopAVLTracking")) {
            isAvlRunning = false;
            listener.stopAVLTracking();
            if (backgroundLocationIntent != null) {
                this.cordova.getActivity().getApplicationContext().stopService(backgroundLocationIntent);
                backgroundLocationIntent = null;
            }
        } else if (action.equals("arrivedTracking")) {
            listener.arrivedTracking();
        } else if (action.equals("preventSleep")) {
            wakeLock.acquire();
        } else if (action.equals("allowSleep")) {
            wakeLock.release();
        } else {
            return false;
        }

        return true;
    }

    public Activity getActivity() {
        return this.cordova.getActivity();
    }

    @SuppressWarnings("deprecation")
	public static void sendJavascript(JSONObject _json, String callback) {
        String _d = "javascript:" + callback + "(" + _json.toString() + ")";
        Log.v(TAG, "sendJavascript: " + _d);

        if (callback != null && gWebView != null) {
            gWebView.sendJavascript(_d);
        }
    }

    @Override
    public void onDestroy() {
        if (listener != null)
            listener.stopTracking();
        if (wakeLock != null)
            wakeLock.release();

        super.onDestroy();
    }

    protected ServiceConnection mServiceConn = new ServiceConnection() {
	    private IBinder _binder;

        @Override
        public void onServiceConnected(ComponentName name, IBinder binder) {
            Log.d(TAG, "onServiceConnected");
            _binder = binder;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            Log.d(TAG, "onServiceDisconnected");
            _binder = null;
        }
    };
}
