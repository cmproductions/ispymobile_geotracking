package com.ispyfire.plugins.geotracking;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaWebView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.util.Log;
import android.os.Bundle;

public class iSpyCurrentLocation extends CordovaPlugin {
    private LocationManager locationManager;
    private iSpyListener listener;
	private static CordovaWebView gWebView;
	private static String TAG = "[iSpyMobile Current Location]";

	public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        if (locationManager == null) {
            locationManager = (LocationManager) this.cordova.getActivity().getSystemService(Context.LOCATION_SERVICE);
        }
        
        if (listener == null) {
            listener = new iSpyListener(locationManager, this);
        }

        if (action.equals("getLocation")) {
        	gWebView = this.webView;
        	listener.startTracking(args.getJSONObject(0));
        } else if (action.equals("startTracking")) {
        	gWebView = this.webView;
        	listener.keepTracking = true;
        	listener.startTracking(args.getJSONObject(0));
        } else if (action.equals("stopTracking")) {
            listener.stopTracking();
        } else {
            return false;
        }

        return true;
    }

    public Activity getActivity() {
        return this.cordova.getActivity();
    }

    @SuppressWarnings("deprecation")
	public static void sendJavascript(JSONObject _json, String callback) {
        String _d = "javascript:" + callback + "(" + _json.toString() + ")";
        Log.v(TAG, "sendJavascript: " + _d);

        if (callback != null && gWebView != null) {
            gWebView.sendJavascript(_d);
        }
    }

    @Override
    public void onDestroy() {
        if (listener != null)
            listener.stopTracking();

        super.onDestroy();
    }
    
    private class iSpyListener extends BroadcastReceiver implements LocationListener {
        public static final String PROX_ALERT_INTENT = "com.ispyfire.plugins.geotracking.CurrentLocationAlert";
        
        protected LocationManager locationManager;
        protected iSpyCurrentLocation ispycurrentlocation;
        protected Boolean running;
        protected String messageCallback;
        protected String locationCallback;
        public    Boolean keepTracking;

        private String TAG = "[iSpyMobile Current Location Listener]";
        
        public iSpyListener(LocationManager manager, iSpyCurrentLocation geo) {
            this.locationManager = manager;
            this.ispycurrentlocation = geo;
            this.running = false;
            this.keepTracking = false;
        }

        public void onProviderDisabled(String provider) {
            Log.d(TAG, "Location provider '" + provider + "' disabled.");
        }
        public void onProviderEnabled(String provider) {
            Log.d(TAG, "Location provider "+ provider + " has been enabled");
        }
        public void onStatusChanged(String provider, int status, Bundle extras) {
            Log.d(TAG, "The status of the provider " + provider + " has changed");
            if (status == 0) {
                Log.d(TAG, provider + " is OUT OF SERVICE");
            }
            else if (status == 1) {
                Log.d(TAG, provider + " is TEMPORARILY_UNAVAILABLE");
            }
            else {
                Log.d(TAG, provider + " is AVAILABLE");
            }
        }

        public void onLocationChanged(Location location) {
            Log.d(TAG, "The location has been updated!");
            Log.d(TAG, "Location: \n" + location.toString());

            if (this.keepTracking) {
                if (location.hasAccuracy() && location.getAccuracy() <= 200)
                {
                    sendLocation(location, this.locationCallback);
                }
            } else {
                if (location.hasAccuracy() && location.getAccuracy() <= 25)
                {
                    sendLocation(location, this.locationCallback);
                    stopTracking();
                }
            }
        }
        
        public void startTracking(JSONObject opts) {
            try
            {
                this.messageCallback = (String) opts.get("mcb");
                this.locationCallback = (String) opts.get("lcb");
            } catch (JSONException e) {
                Log.e(TAG, "startTracking (Current Location): Got JSON Exception " + e.getMessage());
                return;
            }
            
            if (this.running) {
            	stopTracking();
            }

            if (this.locationManager.getProvider(LocationManager.GPS_PROVIDER) != null && !this.running) {
                this.running = true;
                this.locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);

                IntentFilter filter = new IntentFilter(PROX_ALERT_INTENT);
                this.ispycurrentlocation.getActivity().registerReceiver(this, filter);
                sendMessage("Started current location tracking", this.messageCallback);
            } else {
                Log.e(TAG, "GPS provider is not available.");
            }
        }

        public void stopTracking() {
        	Log.d(TAG, "Entering stopTracking");
        	try
        	{
    	        if (this.running) {
	                locationManager.removeUpdates(this);
	                this.running = false;                
		            this.ispycurrentlocation.getActivity().unregisterReceiver(this);
		            this.messageCallback = "";
		            this.locationCallback = "";
		            this.keepTracking = false;
    	            
    	            sendMessage("Stopped current location tracking", this.messageCallback);
    	        }   	
        	}
        	catch (Exception e) {
                sendMessage("Error in stopTracking " + e, this.messageCallback);
                Log.e(TAG, "stopTracking: exception " + e);
                e.printStackTrace();    		
        	}
        }
        
        public void sendMessage(String message, String callback) {
    		try
    		{
    		    JSONObject json = new JSONObject().put("message", message);
    			Log.v(TAG, "sendMessage: " + json.toString());
    			iSpyCurrentLocation.sendJavascript( json, callback );
    		}
    		catch( JSONException e)
    		{
    			Log.e(TAG, "sendMessage: JSON exception");
    		}
        }

        public void sendLocation(Location location, String callback) {
            try
            {
                JSONObject json = new JSONObject().put("course", location.getBearing());
                json.put("altitude", location.getAltitude());
                json.put("speed", location.getSpeed());
                json.put("horizontalAccuracy", location.getAccuracy());
                json.put("longitude", location.getLongitude());
                json.put("latitude", location.getLatitude());
                json.put("timestamp", location.getTime());
                json.put("verticalAccuracy", location.getAccuracy());

                Log.v(TAG, "sendMessage: " + json.toString());
                iSpyCurrentLocation.sendJavascript( json, callback );
            }
            catch( JSONException e)
            {
                Log.e(TAG, "sendMessage: JSON exception");
            }
        }

        @Override
    	public void onReceive(Context context, Intent intent) {
    		Log.d(TAG, "hit onReceive, here for if we where using Proximity Alerts");
    	}
    }
}
