package com.ispyfire.plugins.geotracking;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.webkit.CookieManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;


public class iSpyAVLReceiver extends BroadcastReceiver implements LocationListener {
    public static String PROX_ALERT_INTENT = "com.ispyfire.plugins.geotracking.ProximityAlert";

    protected LocationManager locationManager;
    protected Location latestLocation;
    protected Context context;
    protected Double dblLat;
    protected Double dblLong;
    protected String ispyUrl;
    protected String ispyUserId;
    protected String ispyUserRadioNumber;
    protected String ispyLat;
    protected String ispyLong;
    protected long lastSendTime;
    protected long lastBabysitTime;

    private String TAG = "[iSpyMobile AVL Listener]";

    public static long getNow() {
    	return System.currentTimeMillis()/1000;
    }

    public iSpyAVLReceiver(LocationManager manager, Context context) {
        this.locationManager = manager;
        this.dblLat = (double) 0;
        this.dblLong = (double) 0;
        this.lastSendTime = 0;
        this.lastBabysitTime = iSpyAVLReceiver.getNow();
        this.context = context;
    }
    public void onProviderDisabled(String provider) {
        Log.d(TAG, "Location provider '" + provider + "' disabled.");
    }
    public void onProviderEnabled(String provider) {
        Log.d(TAG, "Location provider "+ provider + " has been enabled");
    }
    public void onStatusChanged(String provider, int status, Bundle extras) {
        Log.d(TAG, "The status of the provider " + provider + " has changed");
        if (status == 0) {
            Log.d(TAG, provider + " is OUT OF SERVICE");
        }
        else if (status == 1) {
            Log.d(TAG, provider + " is TEMPORARILY_UNAVAILABLE");
        }
        else {
            Log.d(TAG, provider + " is AVAILABLE");
        }
    }

    public void onLocationChanged(Location location) {
        Log.d(TAG, "The location has been updated!");
        Log.d(TAG, "Location: \n" + location.toString());
        this.latestLocation = location;
        
        updateiSpy(location, false);
    }

    public boolean startAVLTracking(String url, String userid, String radioNumber) {
        stopTracking();
        Log.d(TAG, "startAVLTracking");
        this.lastSendTime = 0;
        this.ispyUrl = url;
        this.ispyUserId = userid;
        this.ispyUserRadioNumber = radioNumber;

        Log.d(TAG, "ispyUrl:" + this.ispyUrl);
        Log.d(TAG, "ispyUserId:" + this.ispyUserId);
        Log.d(TAG, "ispyRadioNumber:" + this.ispyUserRadioNumber);

        if ((url != null && !url.isEmpty()) && ((userid != null && !userid.isEmpty()) || (radioNumber != null && !radioNumber.isEmpty()))) {
            Log.d(TAG, "have valid inputs");
            if (this.locationManager.getProvider(LocationManager.GPS_PROVIDER) != null) {
                this.locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 500, 0, this);

                IntentFilter filter = new IntentFilter(PROX_ALERT_INTENT);
                context.registerReceiver(this, filter);
                Log.v(TAG, "Started AVL tracking");
                return true;
            } else {
                Log.e(TAG, "GPS provider is not available.");
                return false;
            }
        } else {
            Log.e(TAG, "Don't have proper input, don't start tracking!!!");
            return false;
        }
    }

    public void stopAVLTracking(){
        Log.d(TAG, "stopAVLTracking");
        stopTracking();
    }

    public void stopTracking() {
    	Log.d(TAG, "Entering stopTracking");
    	try
    	{
    	    locationManager.removeUpdates(this);
            this.ispyUrl = "";
            this.ispyUserId = "";
            this.ispyLat = "";
            this.ispyLong = "";
            this.dblLat = (double) 0;
            this.dblLong = (double) 0;
    	}
    	catch (Exception e) {
            Log.e(TAG, "stopTracking: exception " + e);
            e.printStackTrace();    		
    	}
    }

    public void updateiSpy(Location location, Boolean arrived) {
        try
        {
            if (this.lastSendTime == 0 || arrived ||
            		((this.lastSendTime + 30) < iSpyAVLReceiver.getNow()))
            {
                this.lastSendTime = iSpyAVLReceiver.getNow();
                JSONObject json = new JSONObject().put("course", location.getBearing());
                json.put("altitude", location.getAltitude());
                json.put("speed", location.getSpeed());
                json.put("horizontalAccuracy", location.getAccuracy());
                json.put("longitude", location.getLongitude());
                json.put("latitude", location.getLatitude());
                json.put("timestamp", location.getTime() / 1000L);
                json.put("verticalAccuracy", location.getAccuracy());
                json.put("arrived", arrived);
                json.put("userid", this.ispyUserId);
                json.put("unitid", this.ispyUserRadioNumber);
                json.put("isApparatus", true);
                json.put("isAVL", true);
                
                HttpPutParams params = new HttpPutParams(json, this.ispyUrl);
                HttpPutAsync task = new HttpPutAsync();
                task.execute(params);

                Log.v(TAG, "sendIspyUpdate: " + json.toString());
            }
        }
        catch( JSONException e)
        {
            Log.e(TAG, "sendIspyUpdate: JSON exception");
        }
    }
    
    private class HttpPutParams {
    	JSONObject json;
		String ispyUrl;

    	public HttpPutParams(JSONObject json2, String ispyUrl2) {
			this.json = json2;
			this.ispyUrl = ispyUrl2;
		}
		public JSONObject getJson() {
			return json;
		}
		public String getIspyUrl() {
			return ispyUrl;
		}
    }
    
    private class HttpPutAsync extends AsyncTask<HttpPutParams, Void, Void> {
		@Override
		protected Void doInBackground(HttpPutParams... params) {
            URL url = null;
            HttpURLConnection hurl = null;
            try {
                url = new URL(params[0].getIspyUrl());
                hurl = (HttpURLConnection) url.openConnection();
                CookieManager cm = CookieManager.getInstance();
                String cookie = cm.getCookie(params[0].getIspyUrl());
                hurl.setRequestMethod("PUT");
                hurl.setDoOutput(true);
                hurl.setRequestProperty("Content-Type", "application/json");
                hurl.setRequestProperty("Accept", "application/json");
                hurl.setRequestProperty("Cookie", cookie);

                byte[] payload = params[0].getJson().toString().getBytes("UTF-8");
                hurl.setFixedLengthStreamingMode(payload.length);
                hurl.getOutputStream().write(payload);

                
                Log.v(TAG, "sentIspyUpdate to " + params[0].getIspyUrl());
                Log.v(TAG, "updateiSpy: put status " + hurl.getResponseCode());
                Log.v(TAG, "updateiSpy: put body " + new String(hurl.getResponseMessage().getBytes(), "UTF-8"));
            } catch (MalformedURLException e) {
                Log.e(TAG, "updateiSpy: exception " + e);
                e.printStackTrace();
            } catch (IOException e) {
                Log.e(TAG, "updateiSpy: exception " + e);
                e.printStackTrace();
            } catch (Exception e) {
                Log.e(TAG, "updateiSpy: exception (e) " + e);
                e.printStackTrace();               	
            } finally {
                hurl.disconnect();
            }
            
			return null;
		}
    }

	@Override
	public void onReceive(Context context, Intent intent) {
		Log.d(TAG, "hit onReceive, here for if we where using Proximity Alerts");
	}
}
