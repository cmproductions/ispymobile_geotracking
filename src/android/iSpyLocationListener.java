package com.ispyfire.plugins.geotracking;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.webkit.CookieManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.MalformedURLException;


public class iSpyLocationListener extends BroadcastReceiver implements LocationListener {
    public static String PROX_ALERT_INTENT = "com.ispyfire.plugins.geotracking.ProximityAlert";

    protected LocationManager locationManager;
    protected Location latestLocation;
    protected iSpyGeo ispygeo;
    protected Boolean running;
    protected Boolean isapparatus;
    protected Boolean isAVL;
    protected Boolean isAVLArrived;
    protected Boolean isTracking;
    protected Double dblLat;
    protected Double dblLong;
    protected String messageCallback;
    protected String locationCallback;
    protected String ispyUrl;
    protected String ispyUserId;
    protected String ispyUserRadioNumber;
    protected String ispyCallId;
    protected String ispyLongTermCallId;
    protected String ispyLat;
    protected String ispyLong;
    protected String ispyAgency;
    protected long lastSendTime;
    protected long lastBabysitTime;

    private String TAG = "[iSpyMobile Location Listener]";

    public LocationManager getLocationManager() {
        return locationManager;
    }

    public iSpyGeo getIspygeo() {
        return ispygeo;
    }
    
    public static long getNow() {
    	return System.currentTimeMillis()/1000;
    }

    public iSpyLocationListener(LocationManager manager, iSpyGeo geo) {
        this.locationManager = manager;
        this.ispygeo = geo;
        this.running = false;
        this.isAVL = false;
        this.isapparatus = false;
        this.isAVLArrived = false;
        this.isTracking = false;
        this.dblLat = (double) 0;
        this.dblLong = (double) 0;
        this.lastSendTime = 0;
        this.lastBabysitTime = iSpyLocationListener.getNow();
    }
    public void onProviderDisabled(String provider) {
        Log.d(TAG, "Location provider '" + provider + "' disabled.");
    }
    public void onProviderEnabled(String provider) {
        Log.d(TAG, "Location provider "+ provider + " has been enabled");
    }
    public void onStatusChanged(String provider, int status, Bundle extras) {
        Log.d(TAG, "The status of the provider " + provider + " has changed");
        if (status == 0) {
            Log.d(TAG, provider + " is OUT OF SERVICE");
        }
        else if (status == 1) {
            Log.d(TAG, provider + " is TEMPORARILY_UNAVAILABLE");
        }
        else {
            Log.d(TAG, provider + " is AVAILABLE");
        }
    }

    public void onLocationChanged(Location location) {
        Log.d(TAG, "The location has been updated!");
        Log.d(TAG, "Location: \n" + location.toString());
        this.latestLocation = location;
        
        sendLocation(location, false, this.locationCallback);
        updateiSpy(location, false);
        
        if (this.isTracking) {
        	if (containsCoordinate(this.dblLat, this.dblLong, 110, location.getLatitude(), location.getLongitude())){
	            Log.d(TAG, "Entering geofence");
	            sendMessage("Entering geofence", this.messageCallback);
	            this.latestLocation.setLongitude(Double.parseDouble(this.ispyLong));
	            this.latestLocation.setLatitude(Double.parseDouble(this.ispyLat));
	            sendLocation(this.latestLocation, true, this.locationCallback);
	            updateiSpy(this.latestLocation, true);
	            stopTracking();
	            this.isAVLArrived = true;
	            this.isTracking = false;	        	
	        }
        }
        else if (this.isAVLArrived && this.isAVL && !containsCoordinate(this.dblLat, this.dblLong, 110, location.getLatitude(), location.getLongitude())) {
        	this.isAVLArrived = false;
        }
    }
    
    public boolean containsCoordinate(double geoLat, double geoLong, double radius, double pLat, double pLong) {
        float[] results = new float[1];
        Location.distanceBetween(geoLat, geoLong, pLat, pLong, results);
        return results[0] <= radius; 	
    }

    public void startTracking(JSONObject opts) {
        try
        {
        	if (!this.isAVL)
        	{
        		this.lastSendTime = 0;
	            this.messageCallback = (String) opts.get("mcb");
	            this.locationCallback = (String) opts.get("lcb");
	            this.ispyUrl = (String) opts.get("url");
	            this.ispyUserId = (String) opts.get("userid");
	            this.isapparatus = (Boolean) opts.get("isApparatus");
        	}
            this.ispyAgency = (String) opts.get("agency");
	        this.ispyCallId = (String) opts.get("callid");
            this.ispyLongTermCallId = (String) opts.get("longTermCallId");
            this.ispyLat = (String) opts.get("lat");
            this.ispyLong = (String) opts.get("long");
        } catch (JSONException e) {
            Log.e(TAG, "startTracking: Got JSON Exception " + e.getMessage());
            return;
        }

        this.dblLat = Double.parseDouble(this.ispyLat);
        this.dblLong = Double.parseDouble(this.ispyLong);

        if (this.locationManager.getProvider(LocationManager.GPS_PROVIDER) != null && (!this.running || this.isAVL)) {
            this.isTracking = true;
 
            if (!this.running && !this.isAVL){
                this.running = true;
               	this.locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 500, 0, this);
	            IntentFilter filter = new IntentFilter(PROX_ALERT_INTENT);
	            this.ispygeo.getActivity().registerReceiver(this, filter);
            }
            sendMessage("Started tracking", this.messageCallback);
        } else {
            Log.e(TAG, "GPS provider is not available.");
        }
    }

    public void startAVLTracking(JSONObject opts) {
        try
        {
        	this.lastSendTime = 0;
            this.messageCallback = (String) opts.get("mcb");
            this.locationCallback = (String) opts.get("lcb");
            this.ispyUrl = (String) opts.get("url");
            this.ispyUserId = (String) opts.get("userid");
            this.ispyUserRadioNumber = (String) opts.get("userRadioNumber");
            this.isapparatus = (Boolean) opts.get("isApparatus");
        } catch (JSONException e) {
            Log.e(TAG, "startAVLTracking: Got JSON Exception " + e.getMessage());
            return;
        }
        
        if (this.running && !this.isAVL) {
        	stopTracking();
        }

        if (this.locationManager.getProvider(LocationManager.GPS_PROVIDER) != null && !this.running) {
            this.isAVL = true;
            this.running = true;
            this.locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 500, 0, this);

            IntentFilter filter = new IntentFilter(PROX_ALERT_INTENT);
            this.ispygeo.getActivity().registerReceiver(this, filter);
            sendMessage("Started AVL tracking", this.messageCallback);
        } else {
            Log.e(TAG, "GPS provider is not available.");
        }
    }

    public void stopAVLTracking(){
        this.isAVL = false;
        stopTracking();
    }

    public void stopTracking() {
    	Log.d(TAG, "Entering stopTracking");
    	try
    	{
	        if (this.running) {
	            if (!this.isAVL) { 
	                locationManager.removeUpdates(this);
	                this.running = false;                
		            this.ispygeo.getActivity().unregisterReceiver(this);
		            this.isapparatus = false;
		            this.messageCallback = "";
		            this.locationCallback = "";
		            this.ispyUrl = "";
		            this.ispyUserId = "";
	            }
	            
	            this.isTracking = false;
	            this.ispyCallId = "";
	            this.ispyLat = "";
	            this.ispyLong = "";
	            this.dblLat = (double) 0;
	            this.dblLong = (double) 0;
	            
	            sendMessage("Stopped tracking", this.messageCallback);
	        }   	
    	}
    	catch (Exception e) {
            sendMessage("Error in stopTracking " + e, this.messageCallback);
            Log.e(TAG, "stopTracking: exception " + e);
            e.printStackTrace();    		
    	}
    }
    
    public void arrivedTracking(){
    	if (this.isTracking || (this.ispyLat != "" && this.ispyLong != ""))
    	{
	        this.latestLocation.setLongitude(Double.parseDouble(this.ispyLong));
	        this.latestLocation.setLatitude(Double.parseDouble(this.ispyLat));
	        sendLocation(this.latestLocation, true, this.locationCallback);
	        updateiSpy(this.latestLocation, true);
    	}
    	
    	stopTracking();
    }

    public void sendMessage(String message, String callback) {
		try
		{
		    JSONObject json = new JSONObject().put("message", message);
			Log.v(TAG, "sendMessage: " + json.toString());
			iSpyGeo.sendJavascript( json, callback );
		}
		catch( JSONException e)
		{
			Log.e(TAG, "sendMessage: JSON exception");
		}
    }

    public void sendLocation(Location location, Boolean arrived, String callback) {
        try
        {
            JSONObject json = new JSONObject().put("course", location.getBearing());
            json.put("altitude", location.getAltitude());
            json.put("speed", location.getSpeed());
            json.put("horizontalAccuracy", location.getAccuracy());
            json.put("longitude", location.getLongitude());
            json.put("latitude", location.getLatitude());
            json.put("timestamp", location.getTime());
            json.put("verticalAccuracy", location.getAccuracy());
            json.put("arrived", arrived);
            json.put("isAVL", !this.isTracking);

            Log.v(TAG, "sendMessage: " + json.toString());
            iSpyGeo.sendJavascript( json, callback );
        }
        catch( JSONException e)
        {
            Log.e(TAG, "sendMessage: JSON exception");
        }
    }

    public void updateiSpy(Location location, Boolean arrived) {
        try
        {
            if (this.lastSendTime == 0 || arrived ||
            		((this.lastSendTime + 30) < iSpyLocationListener.getNow() && !this.isAVLArrived))
            {
                this.lastSendTime = iSpyLocationListener.getNow();
                JSONObject json = new JSONObject().put("course", location.getBearing());
                json.put("altitude", location.getAltitude());
                json.put("speed", location.getSpeed());
                json.put("horizontalAccuracy", location.getAccuracy());
                json.put("longitude", location.getLongitude());
                json.put("latitude", location.getLatitude());
                json.put("timestamp", location.getTime() / 1000L);
                json.put("verticalAccuracy", location.getAccuracy());
                json.put("arrived", arrived);
                json.put("callid", this.ispyCallId);
                json.put("userid", this.ispyUserId);
                json.put("unitid", this.ispyUserRadioNumber);
                json.put("isApparatus", this.isapparatus);
                json.put("isAVL", !this.isTracking);
                
                HttpPutParams params = new HttpPutParams(json, this.ispyUrl);
                HttpPutAsync task = new HttpPutAsync();
                task.execute(params);

                Log.v(TAG, "sendIspyUpdate: " + json.toString());
                sendMessage("Sending geotracking info to " + this.ispyUrl, this.messageCallback);
            }
            
            if ((this.lastBabysitTime + 90) < iSpyLocationListener.getNow() && this.isTracking && !arrived)
            {
            	this.lastBabysitTime = iSpyLocationListener.getNow();
            	
            	//HttpPutParams params = new HttpPutParams(null, this.ispyUrl + "/" + this.ispyCallId + "?_ispy=" + this.lastBabysitTime);
                HttpPutParams params = new HttpPutParams(null, "https://api.ispyfire.com/calls/isopen/" + this.ispyAgency + "/" + this.ispyLongTermCallId + "?_ispy=" + this.lastBabysitTime);
            	BabyCheckAsync task = new BabyCheckAsync();
            	task.execute(params);
            	Log.v(TAG, "sendIspyUpdate: called BabyCheckAsync");
            }
        }
        catch( JSONException e)
        {
            Log.e(TAG, "sendIspyUpdate: JSON exception");
        }
    }
    
    private class HttpPutParams {
    	JSONObject json;
		String ispyUrl;

    	public HttpPutParams(JSONObject json2, String ispyUrl2) {
			this.json = json2;
			this.ispyUrl = ispyUrl2;
		}
		public JSONObject getJson() {
			return json;
		}
		public String getIspyUrl() {
			return ispyUrl;
		}
    }
    
    private class HttpPutAsync extends AsyncTask<HttpPutParams, Void, Void> {
		@Override
		protected Void doInBackground(HttpPutParams... params) {
            URL url = null;
            HttpURLConnection hurl = null;
            try {
                url = new URL(params[0].getIspyUrl());
                hurl = (HttpURLConnection) url.openConnection();
                CookieManager cm = CookieManager.getInstance();
                String cookie = cm.getCookie(params[0].getIspyUrl());
                hurl.setRequestMethod("PUT");
                hurl.setDoOutput(true);
                hurl.setRequestProperty("Content-Type", "application/json");
                hurl.setRequestProperty("Accept", "application/json");
                hurl.setRequestProperty("Cookie", cookie);

                byte[] payload = params[0].getJson().toString().getBytes("UTF-8");
                hurl.setFixedLengthStreamingMode(payload.length);
                hurl.getOutputStream().write(payload);

                
                Log.v(TAG, "sentIspyUpdate to " + params[0].getIspyUrl());
                Log.v(TAG, "updateiSpy: put status " + hurl.getResponseCode());
                Log.v(TAG, "updateiSpy: put body " + new String(hurl.getResponseMessage().getBytes(), "UTF-8"));
            } catch (MalformedURLException e) {
                Log.e(TAG, "updateiSpy: exception " + e);
                e.printStackTrace();
            } catch (IOException e) {
                Log.e(TAG, "updateiSpy: exception " + e);
                e.printStackTrace();
            } catch (Exception e) {
                Log.e(TAG, "updateiSpy: exception (e) " + e);
                e.printStackTrace();               	
            } finally {
                hurl.disconnect();
            }
            
			return null;
		}
    }
    
    private class BabyCheckAsync extends AsyncTask<HttpPutParams, Void, Integer> {
    	@Override
		protected Integer doInBackground(HttpPutParams... params) {
            URL url = null;
            HttpURLConnection hurl = null;
            try {
                url = new URL(params[0].getIspyUrl());
                hurl = (HttpURLConnection) url.openConnection();
                CookieManager cm = CookieManager.getInstance();
                String cookie = cm.getCookie(params[0].getIspyUrl());
                hurl.setRequestMethod("GET");
                hurl.setDoOutput(false);
                hurl.setRequestProperty("Accept", "application/json");
                hurl.setRequestProperty("Cookie", cookie);

                Log.v(TAG, "BabyCheckAsync: get url " + params[0].getIspyUrl());
                Log.v(TAG, "BabyCheckAsync: get status code " + hurl.getResponseCode());
                Log.v(TAG, "BabyCheckAsync: get body " + new String(hurl.getResponseMessage().getBytes(), "UTF-8"));
                
                return hurl.getResponseCode();
            } catch (MalformedURLException e) {
                Log.e(TAG, "HttpGetAsync: exception " + e);
                e.printStackTrace();
            } catch (IOException e) {
                Log.e(TAG, "HttpGetAsync: exception " + e);
                e.printStackTrace();
            } catch (Exception e) {
                Log.e(TAG, "HttpGetAsync: exception (e) " + e);
                e.printStackTrace();               	
            } finally {
                hurl.disconnect();
            }
            
            return -1;
    	}
    	
        @Override
        protected void onPostExecute(Integer result) {
        	Log.v(TAG, "BabyCheckAsync: onPostExecute");
        	if (result == 404) {
        		Log.v(TAG, "BabyCheckAsync: calling arrived tracking, status code: " + result.toString());
        		arrivedTracking();
        	}
        	else {
        		Log.v(TAG, "BabyCheckAsync: status code: " + result.toString());
        	}
        }
    }

	@Override
	public void onReceive(Context context, Intent intent) {
		Log.d(TAG, "hit onReceive, here for if we where using Proximity Alerts");
	}
}
