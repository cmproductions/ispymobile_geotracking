package com.ispyfire.plugins.geotracking;

import android.app.Activity;
import android.app.Service;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.content.Intent;
import android.content.Context;
import android.content.res.Resources;
import android.location.LocationManager;
import android.preference.PreferenceManager;
import android.util.Log;
import android.app.Notification;
import android.support.v4.app.NotificationCompat;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.DisplayMetrics;


import org.json.JSONException;
import org.json.JSONObject;

public class iSpyAVLService extends Service {
    public LocationManager locationManager;
    public iSpyAVLReceiver listener;
    protected boolean started;
    protected Intent startingIntent;
    protected String ispyUrl;
    protected String ispyUserId;
    protected String ispyRadioNumber;
    private static int FOREGROUND_ID=5675;
    private static String TAG = "[iSpyMobile AVL Service]";
    private static final String PREFERENCE_NAME = "iSpyMobile_AVL";

    Intent intent;
    int counter = 0;

    @Override
    public void onCreate() {
        Log.d(TAG, "onCreate called");
        super.onCreate();
        start();
    }

    @Override
    public void onStart(Intent intent, int startId) {
        Log.d(TAG, "onStart called");
        this.intent = intent;
        start();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "onStartCommand called");
        this.intent = intent;
        start();
        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        started = false;
        super.onDestroy();
        Log.d(TAG, "onDestroy called");
    }

    protected void start() {
        Log.d(TAG, "start called");
        if (this.intent != null) {
            Log.d(TAG, "have intent, grab input and save it");
            try {
                JSONObject opts = new JSONObject(this.intent.getStringExtra("jsonargs"));
                this.ispyUrl = (String) opts.get("url");
                this.ispyUserId = (String) opts.get("userid");
                this.ispyRadioNumber = (String) opts.get("userRadioNumber");
                Log.d(TAG, "ispyUrl:" + this.ispyUrl);
                Log.d(TAG, "ispyUserId:" + this.ispyUserId);
                Log.d(TAG, "ispyRadioNumber:" + this.ispyRadioNumber);

                SharedPreferences pref = getSharedPreferences(PREFERENCE_NAME, Activity.MODE_PRIVATE);
                SharedPreferences.Editor editor = pref.edit();
                editor.putString("url", this.ispyUrl);
                editor.putString("userid", this.ispyUserId);
                editor.putString("userRadioNumber", this.ispyRadioNumber);
                editor.commit();
            } catch (JSONException e) {
                Log.e(TAG, "start: Got JSON Exception " + e.getMessage());
            }
        } else {
            Log.d(TAG, "no intent, try grabbing saved inputs");
            SharedPreferences pref = getSharedPreferences(PREFERENCE_NAME, Activity.MODE_PRIVATE);
            this.ispyUrl = pref.getString("url", null);
            this.ispyUserId = pref.getString("userid", null);
            this.ispyRadioNumber = pref.getString("userRadioNumber", null);
        }

        if (!started) {
            Log.d(TAG, "run continue");
            started = true;
            startForeground(5675, buildForegroundNotification());
            if (locationManager == null) {
                locationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
            }

            if (listener == null) {
                listener = new iSpyAVLReceiver(locationManager, getApplicationContext());
            }

            if (!listener.startAVLTracking(this.ispyUrl, this.ispyUserId, this.ispyRadioNumber)) {
                Log.d(TAG, "stopping self as we don't have the needed input info");
                stopSelf();
            }
        }
    }

    private Notification buildForegroundNotification() {
        NotificationCompat.Builder b=new NotificationCompat.Builder(this);
        Context context = getApplicationContext();

        b.setOngoing(true).setContentTitle("iSpyFire AVL");
        b.setContentText("AVL Tracking is enabled");

        b.setSmallIcon(context.getApplicationInfo().icon);

        Bitmap bm = BitmapFactory.decodeResource(context.getResources(),
                context.getApplicationInfo().icon);

        float mult = getImageFactor(getResources());
        Bitmap scaledBm = Bitmap.createScaledBitmap(bm, (int)(bm.getWidth()*mult), (int)(bm.getHeight()*mult), false);

        if(scaledBm != null) {
            b.setLargeIcon(scaledBm);
        }

        return(b.build());
    }

    private float getImageFactor(Resources r) {
        DisplayMetrics metrics = r.getDisplayMetrics();
        float multiplier=metrics.density/3f;
        return multiplier;
    }
}
