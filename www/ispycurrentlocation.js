var exec = require('cordova/exec');

var ispycurrentlocation = {
    getCurrentLocation:function(locationCallback, errorCallback, options) {
        exec(locationCallback, errorCallback, "iSpyCurrentLocation", "getLocation", [options]);
    },
    startTracking:function(locationCallback, errorCallback, options) {
        exec(locationCallback, errorCallback, "iSpyCurrentLocation", "startTracking", [options]);
    },
    stopTracking:function(locationCallback, errorCallback, options) {
        exec(locationCallback, errorCallback, "iSpyCurrentLocation", "stopTracking", [options]);
    },
};

module.exports = ispycurrentlocation;
