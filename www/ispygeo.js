var exec = require('cordova/exec');

var ispygeo = {
    startTracking:function(locationCallback, errorCallback, options) {
        exec(locationCallback, errorCallback, "iSpyGeo", "startTracking", [options]);
    },
    stopTracking:function(locationCallback, errorCallback, options) {
        exec(locationCallback, errorCallback, "iSpyGeo", "stopTracking", [options]);
    },
    arrivedTracking:function(locationCallback, errorCallback, options) {
        exec(locationCallback, errorCallback, "iSpyGeo", "arrivedTracking", [options]);
    },
    startAVLTracking:function(locationCallback, errorCallback, options) {
        exec(locationCallback, errorCallback, "iSpyGeo", "startAVLTracking", [options]);
    },
    stopAVLTracking:function(locationCallback, errorCallback, options) {
        exec(locationCallback, errorCallback, "iSpyGeo", "stopAVLTracking", [options]);
    },
    preventSleep:function(locationCallback, errorCallback, options) {
        exec(locationCallback, errorCallback, "iSpyGeo", "preventSleep", [options]);
    },
    allowSleep:function(locationCallback, errorCallback, options) {
        exec(locationCallback, errorCallback, "iSpyGeo", "allowSleep", [options]);
    }
};

module.exports = ispygeo;
